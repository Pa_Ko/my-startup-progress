import NodeCache from 'node-cache';

export interface IMemoryFactory {
  getMemory: () => NodeCache;
}

export class MemoryFactory implements IMemoryFactory {
  public getMemory = (): NodeCache => new NodeCache();
}
