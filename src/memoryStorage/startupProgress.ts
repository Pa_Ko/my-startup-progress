import { IPoint, IProgressHeader, IStartupProgress } from '../entities';
import NodeCache from 'node-cache'; //I have never really used in memory storage so I just chosed one that seemed the most simple

export interface IStartupProgressMemory {
  addHeader: (headerText: string) => IStartupProgress;
  removeHeader: (headerText: string) => IStartupProgress;
  addPointToHeader: (headerText: string, pointText: string, pointIsDone: boolean) => IStartupProgress;
  removePointFromHeader: (headerText: string, pointText: string) => IStartupProgress;
  getProgress: () => IStartupProgress;
  updatePoint: (headerText: string, pointText: string, pointIsDone: boolean) => IStartupProgress;
}

export class StartupProgressMemory implements IStartupProgressMemory {
  private key = 'StartupProgress';
  public constructor(private memory: NodeCache) {
    try {
      this.getProgress()
    } catch (error) {
      const defaultProgress: IStartupProgress = {
        progressHeaders: [
          {
            text: 'Foundation',
            points: [
              {
                text: 'Setup virtual office',
                isDone: true,
              },
              {
                text: 'Set mission & vission',
                isDone: true,
              },
              {
                text: 'Select business name',
                isDone: true,
              },
              {
                text: 'Buy domains',
                isDone: true,
              },
            ]
          },
          {
            text: 'Discovery',
            points: [
              {
                text: 'Create roadmap',
                isDone: true,
              },
              {
                text: 'Competitor analysis',
                isDone: false,
              },
            ]
          },
          {
            text: 'Delivery',
            points: [
              {
                text: 'Release marketing website',
                isDone: false,
              },
              {
                text: 'Release MVP',
                isDone: false,
              },
            ]
          },
        ]
      }
      this.memory.set(this.key, defaultProgress)
    }
  }

  public addHeader = (headerText: string): IStartupProgress => {
    const progress = this.takeProgress();

    // todo add validation for duplicity text else push

    progress.progressHeaders.push({
      text: headerText,
      points: [],
    });

    this.memory.set(this.key, progress)

    return this.getProgress();
  }
  public removeHeader = (headerText: string): IStartupProgress => {
    const progress = this.takeProgress();

    const removedHeader = progress.progressHeaders.filter((h: IProgressHeader): boolean => h.text !== headerText);

    const newProgress = {
      progressHeaders: removedHeader,
    }

    this.memory.set(this.key, newProgress)

    return this.getProgress();
  }

  public addPointToHeader = (headerText: string, pointText: string, pointIsDone: boolean): IStartupProgress => {
    const progress = this.takeProgress();

    const updatedHeaders = progress.progressHeaders.map((h: IProgressHeader): IProgressHeader => {
      if (h.text === headerText) {
        // todo validate for duplicity
        h.points.push({
          text: pointText,
          isDone: pointIsDone,
        });
      }
      return h
    });
    const newProgress = {
      progressHeaders: updatedHeaders,
    }

    this.memory.set(this.key, newProgress)

    return this.getProgress();
  };

  public removePointFromHeader = (headerText: string, pointText: string): IStartupProgress => {
    const progress = this.takeProgress();

    const updatedHeaders = progress.progressHeaders.map((h: IProgressHeader): IProgressHeader => {
      if (h.text === headerText) {
        const removedPoint = h.points.filter((p: IPoint): boolean => p.text !== pointText);
        return {
          text: h.text,
          points: removedPoint,
        }
      }
      return h
    });
    const newProgress = {
      progressHeaders: updatedHeaders,
    }

    this.memory.set(this.key, newProgress)

    return this.getProgress();
  };

  public updatePoint = (headerText: string, pointText: string, pointIsDone: boolean): IStartupProgress => {
    const progress = this.takeProgress();

    const updatedHeaders = progress.progressHeaders.map((h: IProgressHeader): IProgressHeader => {
      if (h.text === headerText) {
        const updatedPoints = h.points.map((p: IPoint): IPoint => {
          if (p.text === pointText) {
            return {
              text: p.text,
              isDone: pointIsDone,
            }
          }
          return p;
        });
        return {
          text: h.text,
          points: updatedPoints,
        }
      }
      return h
    });
    const newProgress = {
      progressHeaders: updatedHeaders,
    }

    this.memory.set(this.key, newProgress)

    return this.getProgress();
  }

  public getProgress = (): IStartupProgress => {
    const progress = this.memory.get<IStartupProgress>(this.key)
    if (!progress) throw new Error('No progress found')
    return progress
  };

  private takeProgress = (): IStartupProgress => {
    const progress = this.memory.take<IStartupProgress>(this.key)
    if (!progress) throw new Error('No progress found')
    return progress
  }
}
