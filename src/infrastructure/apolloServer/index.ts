import { ApolloServer, gql } from 'apollo-server';
import { MemoryFactory, StartupProgressMemory } from '../../memoryStorage';

const typeDefs = gql`
  type Point {
    text: String
    isDone: Boolean
  }

  type ProgressHeader {
    text: String
    points: [Point]
  }

  type EnvelopedHeaders {
    progressHeaders: [ProgressHeader]
  }

  type Query {
    getStartupProgress: EnvelopedHeaders
  }

  type Mutation {
    addHeader(headerText: String): EnvelopedHeaders
    removeHeader(headerText: String): EnvelopedHeaders
    addPointToHeader(headerText: String, pointText: String, pointIsDone: Boolean): EnvelopedHeaders
    updatePoint(headerText: String, pointText: String, pointIsDone: Boolean): EnvelopedHeaders
    removePointFromHeader(headerText: String, pointText: String): EnvelopedHeaders
  }
`
// everything would be initialized in a container file and access via dependency injection to avoid spaghetti
const memory = new MemoryFactory().getMemory();
const startupProgress = new StartupProgressMemory(memory);

const resolvers = {
  Query: {
    getStartupProgress: () => {
      return startupProgress.getProgress();
    },
  },
  Mutation: { // todo add arguments validator + correct types
    addHeader: (_parent: any, args: any) =>  startupProgress.addHeader(args.headerText),
    removeHeader: (_parent: any, args: any) => startupProgress.removeHeader(args.headerText),
    addPointToHeader: (_parent: any, args: any) => startupProgress.addPointToHeader(args.headerText, args.pointText, args.pointIsDone),
    updatePoint: (_parent: any, args: any) => startupProgress.updatePoint(args.headerText, args.pointText, args.pointIsDone),
    removePointFromHeader: (_parent: any, args: any) => startupProgress.removePointFromHeader(args.headerText, args.pointText),
  },
};

export const server = new ApolloServer({ typeDefs, resolvers});
