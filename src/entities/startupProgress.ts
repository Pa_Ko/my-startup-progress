export interface IStartupProgress {
  progressHeaders: IProgressHeader[]
}

export interface IProgressHeader {
  text: string;
  points: IPoint[];
}

export interface IPoint {
  text: string;
  isDone: boolean;
}

