import { server } from './infrastructure/';

server.listen().then(({url}) => {
  console.log(`Server running on port ${url}`);
})
